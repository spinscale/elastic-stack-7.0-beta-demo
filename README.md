# Elastic Stack Demo Environment

This demo environment is intended to show off a variety of features

* Dev-Tools demo
* Indexing data coming from a websocket stream via filebeat into Elasticsearch
* Indexing data coming from a HTTP stream via filebeat into Elasticsearch
* Update App via heartbeat

## Prerequisites

* docker-compose
* local filebeat instance, use 7.0.0-beta1 at least
* websocat

## Demos

Fire up docker-compose, then open up dev-tools, then start additional
filebeats

```
# this starts Elasticsearch, kibana, heartbeat, metricbeat
docker-compose up
```

In dev-tools

```
# Adds a pipeline for meetups
PUT _ingest/pipeline/meetups_pipeline
{
  "processors": [
    {
      "script": {
        "lang": "painless",
        "source": """
if (ctx.group != null && ctx.group.group_lat != null && ctx.group.group_lon != null) {
  ctx.group.location = ["lat": ctx.group.group_lat, "lon":ctx.group.group_lon];
  ctx.group.remove("group_lon");
  ctx.group.remove("group_lat");
}
if (ctx.venue != null && ctx.venue.lon != null && ctx.venue.lat != null) {
  ctx.venue.location = ["lat": ctx.venue.lat, "lon":ctx.venue.lon];
  ctx.venue.remove("lon");
  ctx.venue.remove("lat");
}
ctx.remove("agent");
ctx.remove("log");
ctx.remove("ecs");
ctx.remove("input");
ctx.remove("host");
ctx.remove("mtime");
"""
      }
    }
  ]
}

PUT _template/meetups
{
  "index_patterns" : [ "meetups-*" ],
  "mappings" : {
    "properties" : {
      "venue" : {
        "properties" : {
          "location": {
            "type":"geo_point"
          }
        }
      },
      "group" : {
        "properties" : {
          "location": {
            "type":"geo_point"
          }
        }
      }
    }
  }
}
```

Start filebeats in separate console

```
curl -s http://stream.meetup.com/2/rsvps |  ./filebeat-7.0.0-beta1-darwin-x86_64/filebeat run -c filebeat-meetup.yml -e

websocat wss://certstream.calidog.io | ./filebeat-7.0.0-beta1-darwin-x86_64/filebeat -c filebeat-certstream.yml -e
```


### Show Dev-Tools with meetup.com data

```
GET _cat/indices

# Mapping, Settings
GET meetups-*


GET meetups-*/_search
{
  "size": 1
}

# Search via ?q=
GET meetups-*/_count

GET meetups-*/_search?q=STH

# Search via body
GET meetups-2019.02/_search
{
  "query": {
    "match": {
      "group.group_topics.urlkey": "STH"
    }
  }
}

GET meetups-2019.02/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "group.group_topics.urlkey": "software-testing"
          }
        },
        {
          "match": {
            "group.group_country": "de"
          }
        }      
      ]
    }
  }
}

# Aggregations
GET meetups-*/_search
{
  "size": 0,
  "aggs": {
    "by_country": {
      "terms": {
        "field": "group.group_country.keyword"
      }
    }
  }
}

GET meetups-*/_search
{
  "size": 0,
  "aggs": {
    "by_guest": {
      "histogram": {
        "field": "guests",
        "interval": 1
      }
    }
  }
}

GET meetups-2019.02/_search
{
  "size": 0,
  "aggs": {
    "by_venue_id": {
      "terms": {
        "field": "venue.venue_id",
        "size": 1
      },
      "aggs": {
        "top_venue": {
          "top_hits": {
            "size": 1,
            "_source": {
              "includes": [
                "venue.*"
              ]
            }
          }
        }
      }
    }
  }
}

# Highlighting
GET meetups-*/_search
{
  "query": {
    "bool": {
      "should": [
        {
          "match": {
            "event.event_name": "Elastic"
          }
        },
        {
          "match": {
            "event.event_name": "Elasticsearch"
          }
        }
      ]
    }
  },
  "highlight": {
    "fields": {
      "event.event_name": {}
    }
  }
}

# certstream data
GET certstream-*/_search
{
  "size": 0,
  "aggs": {
    "by_subject": {
      "terms": {
        "field": "data.leaf_cert.subject.CN.keyword",
        "size": 10
      }
    }
  }
}
```

### Show Heartbeat/Uptime App

Show uptime App plus underlying data in dev-tools.

### Show meetup.com stream

* Use the maps app to visualize
* Create a meetups dashboard yourself, or import `meetups-dashboard.json`
* Use the Visualize tab to check out meetup data


## TODO

